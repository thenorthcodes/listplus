package com.android.listplus;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;

import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddActivity extends AppCompatActivity {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    AppCompatButton appCompatButton;
    TextInputEditText name, qty, shop;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        dbHelper = new DBHelper(getApplicationContext());
        appCompatButton = (AppCompatButton) findViewById(R.id.send);
        name = (TextInputEditText) findViewById(R.id.name);
        qty = (TextInputEditText) findViewById(R.id.qty);
        shop = (TextInputEditText) findViewById(R.id.shop);

        firebaseDatabase= FirebaseDatabase.getInstance();
        databaseReference =firebaseDatabase.getReference().child("List");

        appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DataModel dataModel = new DataModel(name.getText().toString(), qty.getText().toString(), shop.getText().toString());
                //databaseReference.push().setValue(dataModel);*/
                dbHelper.addToCart(dataModel);
                setResult(1, new Intent());
                finish();
            }
        });
    }
}
