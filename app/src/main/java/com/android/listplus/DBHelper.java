package com.android.listplus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arti on 1/1/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "shoplist";
    // order table name
    private static final String TABLE_LIST = "list";

    // order Table Columns names
    private static final String PROD_NAME = "name";
    private static final String PROD_QTY= "quantity";
    private static final String PROD_SHOP = "shop";




    String CREATE_LIST_TABLE = "CREATE TABLE " + TABLE_LIST + " (" + "uni_id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PROD_NAME +" TEXT,"
            + PROD_QTY + " STRING,"+ PROD_SHOP + " TEXT"+ ")";



    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_LIST_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);


// Creating tables again
        onCreate(db);
    }


    public void addToCart(DataModel getDataAdapter){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PROD_NAME, getDataAdapter.getName());
        values.put(PROD_QTY, getDataAdapter.getNumber());
        values.put(PROD_SHOP, getDataAdapter.getShop());
        // Inserting Row
        db.insert(TABLE_LIST, null, values);
        db.close();
    }

    public void removeFromCart(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LIST, PROD_NAME+"=?", new String[]{name});
        db.close();
    }


    public List<DataModel> getAllCartItems(){
        String[] columns = {
                PROD_NAME,
                PROD_QTY,
                PROD_SHOP
        };

        String sortOrder = PROD_NAME + " ASC";
        List<DataModel> getDataAdapterList = new ArrayList<DataModel>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LIST, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order


        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DataModel getDataAdapter = new DataModel();
                getDataAdapter.setName(cursor.getString(cursor.getColumnIndex(PROD_NAME)));
                getDataAdapter.setNumber(cursor.getString(cursor.getColumnIndex(PROD_QTY)));
                getDataAdapter.setShop(cursor.getString(cursor.getColumnIndex(PROD_SHOP)));
                // Adding user record to list
                getDataAdapterList.add(getDataAdapter);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return getDataAdapterList;

    }
}
