package com.android.listplus;

/**
 * Created by Arti on 12/24/2017.
 */

public class DataModel {

    String name, number, shop, key;

    public DataModel() {
    }

    public DataModel(String name, String number, String shop) {
        this.name = name;
        this.number = number;
        this.shop = shop;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getShop() {
        return this.shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }
}
