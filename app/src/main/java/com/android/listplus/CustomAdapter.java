package com.android.listplus;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arti on 12/24/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private List<DataModel> dataSet;




    public CustomAdapter(List<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int listPosition) {
        TextView textViewName = holder.textViewName;
        TextView textViewNumber = holder.textViewNumber;
        TextView textViewShop = holder.textViewShop;

        textViewName.setText(dataSet.get(listPosition).getName());
        textViewNumber.setText(dataSet.get(listPosition).getNumber());
        textViewShop.setText(dataSet.get(listPosition).getShop());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewNumber;
        TextView textViewShop;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName =  itemView.findViewById(R.id.textViewName);
            this.textViewNumber =  itemView.findViewById(R.id.textViewNumber);
            this.textViewShop =  itemView.findViewById(R.id.textViewShop);
        }
    }
}
